import Component from '@ember/component';

export default Component.extend({
  willDestroyElement() {
    this._super(...arguments);
    this.sendAction('spinnerDestroyed');
  },
});
