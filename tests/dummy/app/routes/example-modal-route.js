import Route from '@ember/routing/route';
import RouteOpenModalMixin from 'ember-route-modal/mixins/route-open-modal-mixin'

export default Route.extend(RouteOpenModalMixin, {
  modalId                     : 'exampleModal',
  transitionOnModalClose      : 'application',
  transitionToObjectId        : null
});
