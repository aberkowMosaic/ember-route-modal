import { scheduleTask, runTask, runDisposables } from 'ember-lifeline';
import Mixin from '@ember/object/mixin';
import $ from 'jquery';

export default Mixin.create({
  getModalId() {
    return '#' + this.getWithDefault('modalId', 'defaultModalId');
  },

  // this is called immediately when the route is exited
  // hotfix for the browser back button keeping the modal backdrop visible
  deactivate() {
    if (!this.get('disableBackdropCleanup')) {
      $('.modal-backdrop').remove();
    }

    document.body.className = 'ember-application';
    document.body.style.cssText = '';
  },

  // clean-up any issues as a result of scheduled or run tasks
  didDestroy() {
    runDisposables(this);
  },

  actions: {
    openModal() {
      var modalId = this.getModalId();

      document.body.className = 'ember-application modal-open';
      document.body.style.cssText = 'padding-right: 15px';

      scheduleTask(this, 'actions', () => {
        $(modalId).modal('show');
        $(modalId).on('hidden.bs.modal', () => {
          //Optional transition specified by route on modal close
          if (this.get('transitionOnModalClose')) {
            let transitionRoute = this.get('transitionOnModalClose');
            let transitionParams = this.get('transitionToObjectId');

            // handle route transitions that contain multiple parameters
            if (transitionParams && Array.isArray(transitionParams)) {
              this.transitionTo(transitionRoute, ...transitionParams);

            // handle route transitions that only have one parameter
            } else if (transitionParams) {
              this.transitionTo(transitionRoute, transitionParams);

            // handle route transitions with no parameters
            } else {
              this.transitionTo(transitionRoute);
            }
          }
        });
      });
    },

    cancel() {
      $('#' + this.get('modalId')).modal('hide');
    },

    didTransition() {
      runTask(this, () => {
        this.send('openModal');
      });
    },
  }
});
